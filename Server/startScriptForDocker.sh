# Set WebService ports
sed  -i "s/^server.port.*/server.port=$HTTPS_PORT/g"             /mikemeter/src/main/resources/application.properties
sed  -i "s/^http.port.*/http.port=$HTTP_PORT/g"             /mikemeter/src/main/resources/application.properties

(bash -c 'sleep 10 && cd /mikemeter/ && authbind --deep ./gradlew bootRun')

