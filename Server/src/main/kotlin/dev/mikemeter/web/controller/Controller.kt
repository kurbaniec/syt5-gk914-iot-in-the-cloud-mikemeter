package dev.mikemeter.web.controller

import okhttp3.OkHttpClient
import org.influxdb.BatchOptions
import org.influxdb.InfluxDBFactory
import org.influxdb.dto.Point
import org.influxdb.dto.Query
import org.influxdb.dto.QueryResult
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Serves the REST-Api that the Raspi uses to transfer measurement to the server
 *
 * @author Kacper Urbaniec & Martin Wustinger
 * @version 2019-10-17
 */

@Controller
@CrossOrigin(value = ["*"])
class Controller(private val properties: Properties) {
    private val influxDB = InfluxDBFactory.connect("http://localhost:8086", OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .retryOnConnectionFailure(true))
    private val dbName = "mikemeter"
    private var rate = properties.getProperty("raspberry.samplingrate").toLong()
    private var tmpth = properties.getProperty("tmp.threshold").toInt()
    private var humth = properties.getProperty("hum.threshold").toInt()

    init {
        //influxDB.deleteDatabase(dbName)
        influxDB.createDatabase(dbName)
        influxDB.setDatabase(dbName)
        influxDB.enableBatch(BatchOptions.DEFAULTS)
    }

    @RequestMapping(value = ["/", "/humidity", "/temperature", "/admin"])
    fun app(): String {
        return "index"
    }

    @PostMapping(value = ["/rate"])
    @ResponseBody
    fun rate(@RequestBody l: Long): Long {
        println("--New-Sample_Rate: rate: $l")
        rate = if (l<=0L) rate else l
        return rate
    }

    @PostMapping(value = ["/tmpth"])
    @ResponseBody
    fun tmpth(@RequestBody i: Int): Int {
        println("--New-Tmp_Threshold: tmp: $i")
        tmpth = if (i<=0) tmpth else i
        return tmpth
    }

    @PostMapping(value = ["/humth"])
    @ResponseBody
    fun humth(@RequestBody i: Int): Int {
        println("--New-Hum_Threshold: hum: $i")
        humth = if (i<=0) humth else i
        return humth
    }

    @PostMapping(value = ["/add"])
    @ResponseBody
    fun add(@RequestBody m: Measurement): Long {
        if (m.time == 0L)
            m.time = System.currentTimeMillis()
        println("--New-Measurement: time: ${m.time}, tmp: ${m.tmp}, hum: ${m.hum}")
        influxDB.write(Point.measurement("tmp")
                .time(m.time, TimeUnit.MILLISECONDS)
                .addField("tmp", m.tmp)
                .build())
        influxDB.write(Point.measurement("hum")
                .time(m.time, TimeUnit.MILLISECONDS)
                .addField("hum", m.hum)
                .build())
        return rate
    }

    @PostMapping(value = ["/query"])
    @ResponseBody
    fun query(@RequestBody p: Period): QueryResult? {
        println("--New-Query: type: ${p.type}, time: ${p.time}, last: ${p.last}")
        val selector = if (p.last)  "LAST(*)" else "*"
        val timing = if (p.last) "" else "WHERE time >= '${p.time}'"
        val query = Query("SELECT $selector FROM ${p.type} $timing", dbName)
        return influxDB.query(query)
    }
}