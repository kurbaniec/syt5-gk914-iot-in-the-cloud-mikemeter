package dev.mikemeter.web.controller

class Measurement(var time: Long = System.currentTimeMillis(), var tmp: Double, var hum: Double)