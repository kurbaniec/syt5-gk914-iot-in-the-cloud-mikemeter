const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyWebPackPlugin = require("copy-webpack-plugin");
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
const os = require('os');
const path = require('path');
const fs = require('fs');
const fsExtra = require('fs-extra');
const Q = require('q');
const Dotenv = require('dotenv-webpack');

/**
 * Create .env file if it does not exist
 */
if (!fs.existsSync( '.env')) {
  fs.appendFileSync('.env', 'PORT=8080' + os.EOL);
  fs.appendFileSync('.env', 'OPEN=true'+ os.EOL);
  fs.appendFileSync('.env', 'INFLUXDEV=http://localhost:8086' + os.EOL);
  fs.appendFileSync('.env', 'SPRING=false' + os.EOL);
}
const env = require('dotenv').config({path: __dirname + '/.env'}).parsed;

// Configure React-App
const appConfig = {
  entry: {
    'index': './src/index.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
        loader: 'url-loader?limit=100000'
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./public/index.html",
      filename: "index.html",
      excludeAssets: [/backendtest.*.js/]
    }),
    new HtmlWebpackExcludeAssetsPlugin(),
    new CopyWebPackPlugin([
      { from: "public", ignore: ["index.html"] }
    ]),
    new Dotenv()
  ],
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: '/'
  },
  devServer: {
    historyApiFallback: true,
    port: env.PORT,
    open: (env.OPEN === 'true' || env.OPEN === 'TRUE')
  },
  mode: "production"
};

const workerConfig = {
  entry: "./src/worker.js",
  target: "webworker",

  resolve: {
    extensions: [".js", ".wasm"]
  },
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: '/',
    filename: "worker.js"
  },
  plugins: [
    new CopyWebPackPlugin([
      {from: "modules", to: "modules"}
    ]),
    new Dotenv()
  ]
};

module.exports = [appConfig, workerConfig];

