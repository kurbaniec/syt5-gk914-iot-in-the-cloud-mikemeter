import * as React from "react";
import {
    ThemeProvider, createTheme, Button, Arwes, Row, Col, Content,
    Frame, Words, Heading
} from 'arwes';
import '../style.css'
import { withRouter, Redirect } from "react-router-dom";
import Header from './header';

class Config extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tmpTH: null,
            volTH: null,
            rate: null
        };
        this.workerCall = this.workerCall.bind(this);
        this.handleRateChange = this.handleRateChange.bind(this);
        this.handleTmpChange = this.handleTmpChange.bind(this);
        this.handleVolChange = this.handleVolChange.bind(this);
        this.update = this.update.bind(this);
    }

    /**
     * Bind Web Worker message event
     */
    componentDidMount() {
        this.props.worker.addEventListener('message', this.workerCall, false);
        this.props.worker.postMessage(['getConfig']);
    }

    /**
     * Unbind Web Worker message event
     */
    componentWillUnmount() {
        this.props.worker.postMessage(['clear']);
        this.props.worker.removeEventListener('message', this.workerCall, false);
        console.log("Worker-Event removed");
    }

    /**
     * Respond to Web Worker messages
     * @param e Event containing send data
     */
    workerCall(e) {
        const cmd = e.data[0];
        const data = e.data[1];
        if (cmd === 'config') {
            this.setState({
                tmpTH: data.tmpTH,
                humTH: data.humTH,
                rate: data.rate,
            });
        }
    }

    handleRateChange(e) {
        this.setState({rate: e.target.value});
    }

    handleTmpChange(e) {
        this.setState({tmpTH: e.target.value});
    }

    handleVolChange(e) {
        this.setState({humTH: e.target.value});
    }

    update() {
        const data = {
          tmpTH: this.state.tmpTH,
          humTH: this.state.humTH,
          rate: this.state.rate
        };
        this.props.worker.postMessage(['config', data]);
    }

    componentDidCatch(error, errorInfo) {
        console.log(error, errorInfo);
    }

    render() {
        return (
            <ThemeProvider theme={createTheme}>
                <Arwes animate show>
                    {anim => (
                        <div className="MikeMeter">
                            <Header anim={anim} history={this.props.history}/>
                            <Content style={{marginTop: 30}}><h1>Configure MikeMeter</h1></Content>
                            <Row>
                                <Col s={12} style={{padding: 0}}>
                                    <Frame animate show={anim.entered} level={1} corners={3}>
                                        <Content style={{ padding: 20 }}>
                                            <Col s={12} style={{padding: 10}}>
                                                <h3 style={{display: 'inline-block', 'margin-right': 10}}>Rate</h3>
                                                <input type="number" value={this.state.rate}
                                                    onChange={this.handleRateChange}/>
                                            </Col>
                                            <Col s={12} style={{padding: 10}}>
                                                <h3 style={{display: 'inline-block', 'margin-right': 10}}>Tmp-Threshold</h3>
                                                <input type="number" value={this.state.tmpTH}
                                                   onChange={this.handleTmpChange}/>
                                            </Col>
                                            <Col s={12} style={{padding: 10}}>
                                                <h3 style={{display: 'inline-block', 'margin-right': 10}}>Hum-Threshold</h3>
                                                <input type="number" value={this.state.humTH}
                                                   onChange={this.handleVolChange}/>
                                            </Col>
                                            <Col s={12} style={{padding: 10}}>
                                                <Button onClick={this.update}>Update</Button>
                                            </Col>
                                        </Content>
                                    </Frame>
                                </Col>
                            </Row>
                        </div>
                    )}
                </Arwes>
            </ThemeProvider>
        );
    }
}

export default withRouter(Config);