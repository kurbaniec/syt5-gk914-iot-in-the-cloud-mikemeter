import * as React from "react";
import Calendar from 'react-calendar';
import { withRouter } from "react-router-dom";
import {
    ThemeProvider, createTheme, Button, Arwes, Row, Col, Content,
    Frame, Words, Image
} from 'arwes';
import '../style.css'
import Header from "./header";
import Plotly from "plotly.js-dist";
import {tmpLayout, lineColor} from "../plot/layout";
import Inhale from '../img/inhale.png';
import Scream from '../img/scream.png';

class Temperature extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 10,
            date: new Date(),
            status: null,
            readings: {
                time: [],
                value: []
            }
        };
        this.workerCall = this.workerCall.bind(this);
        this.plot = this.plot.bind(this);
        this.request = this.request.bind(this);
    }

    /**
     * Bind Web Worker message event
     */
    componentDidMount() {
        this.plot(null);
        this.props.worker.addEventListener('message', this.workerCall, false);
        this.props.worker.postMessage(['tmp']);
        window.addEventListener('resize', this.plot);
    }

    /**
     * Unbind Web Worker message event
     */
    componentWillUnmount() {
        this.props.worker.postMessage(['clear']);
        this.props.worker.removeEventListener('message', this.workerCall, false);
        window.addEventListener('resize', this.plot);
        console.log("Worker-Event removed");
    }

    /**
     * Respond to Web Worker messages
     * @param e Event containing send data
     */
    workerCall(e) {
        const cmd = e.data[0];
        const data = e.data[1];
        if (cmd === 'tmp') {
            this.setState({
                status: data.status,
                readings: data.readings
            }, () => this.plot())
        }
    }

    /**
     * Plot measurements
     */
    plot() {
        const data = [{
            x: this.state.readings.time,
            y: this.state.readings.value,
            type: 'scatter',
            line: lineColor
        }];
        Plotly.newPlot(this.element, data, tmpLayout);
    }

    /**
     * Request plot with new time range
     */
    request(date) {
        this.setState({date: date}, () => {
            this.props.worker.postMessage(['time', new Date(this.state.date.getTime())]);
        });
    }

    render() {
        return (
            <ThemeProvider theme={createTheme}>
                <Arwes animate show>
                    {anim => (
                        <div className="MikeMeter">
                            <Header anim={anim} history={this.props.history}/>
                            <Content style={{marginTop: 30}}><h1>Temperature</h1></Content>
                            <Row>
                                <Col s={12} l={6} style={{padding: 0}}>
                                    <Frame animate show={anim.entered} level={1} corners={3}>
                                        <Content style={{ padding: 20 }}>
                                            <h1>Status</h1>
                                            {this.state.status === null && (
                                                <p>No Data found</p>
                                            )}
                                            {this.state.status === true && (
                                                <Image animate resources={Inhale}>
                                                    <p>{/* Emoji as surrogate pair*/'\uD83D\uDC4D'}</p>
                                                </Image>
                                            )}
                                            {this.state.status === false && (
                                                <Image animate resources={Scream}>
                                                    <p>{'\uD83D\uDC4E'}</p>
                                                </Image>
                                            )}
                                        </Content>
                                    </Frame>
                                </Col>
                                <Col s={12} l={6} style={{padding: 0}}>
                                    <Frame animate show={anim.entered} level={1} corners={3}>
                                        <Content style={{ padding: 20 }}>
                                            <h1>Plot</h1>
                                            <p>Set time point for measurement readings</p>
                                            <div className="filter">
                                                {/*
                                                <input className="Plot" type="Number" min="1" max="24"
                                                   value={this.state.value}
                                                   onChange={filter => this.plot(filter)}/> */}
                                                <Calendar className="Calendar"
                                                    maxDate={new Date()}
                                                    onChange={this.request}
                                                    value={this.state.date}
                                                />
                                            </div>
                                            <div ref={element => this.element = element}></div>
                                        </Content>
                                    </Frame>
                                </Col>
                            </Row>
                        </div>
                    )}
                </Arwes>
            </ThemeProvider>
        );
    }
}


export default withRouter(Temperature);