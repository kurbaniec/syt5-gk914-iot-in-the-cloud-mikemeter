import * as React from "react";
import {
    ThemeProvider, createTheme, Button, Arwes, Row, Col, Content,
    Frame, Words, Heading, Image
} from 'arwes';
import '../style.css'
import { withRouter, Redirect } from "react-router-dom";
import Header from './header';
import Inhale from '../img/inhale.png';
import Scream from '../img/scream.png';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: {
                tmp: null,
                hum: null
            }
        };
        this.workerCall = this.workerCall.bind(this);
    }

    /**
     * Bind Web Worker message event
     */
    componentDidMount() {
        this.props.worker.addEventListener('message', this.workerCall, false);
        this.props.worker.postMessage(['home']);
    }

    /**
     * Unbind Web Worker message event
     */
    componentWillUnmount() {
        this.props.worker.postMessage(['clear']);
        this.props.worker.removeEventListener('message', this.workerCall, false);
        console.log("Worker-Event removed");
    }

    /**
     * Respond to Web Worker messages
     * @param e Event containing send data
     */
    workerCall(e) {
        const cmd = e.data[0];
        const data = e.data[1];
        if (cmd === 'home') {
            this.setState({status: data})
        }
    }

    componentDidCatch(error, errorInfo) {
        console.log(error, errorInfo);
    }

    render() {
        return (
            <ThemeProvider theme={createTheme}>
                <Arwes animate show>
                    {anim => (
                        <div className="MikeMeter">
                            <Header anim={anim} history={this.props.history}/>
                            <Content style={{marginTop: 30}}><h1>Current Status</h1></Content>
                            <Row>
                                <Col s={12} l={6} style={{padding: 0}}>
                                    <Frame animate show={anim.entered} level={1} corners={3}>
                                        <Content style={{ padding: 20 }}>
                                            <h1>Temperature</h1>
                                            {this.state.status.tmp === null && (
                                                <p>No Data found</p>
                                            )}
                                            {this.state.status.tmp === true && (
                                                <Image animate resources={Inhale}>
                                                    <p>{/* Emoji as surrogate pair*/'\uD83D\uDC4D'}</p>
                                                </Image>
                                            )}
                                            {this.state.status.tmp === false && (
                                                <Image animate resources={Scream}>
                                                    <p>{'\uD83D\uDC4E'}</p>
                                                </Image>
                                            )}
                                        </Content>
                                    </Frame>
                                </Col>
                                <Col s={12} l={6} style={{padding: 0}}>
                                    <Frame animate show={anim.entered} level={1} corners={3}>
                                        <Content style={{ padding: 20 }}>
                                            <h1>Humidity</h1>
                                            {this.state.status.hum === null && (
                                                <p>No Data found</p>
                                            )}
                                            {this.state.status.hum === true && (
                                                <Image animate resources={Inhale}>
                                                    <p>{/* Emoji as surrogate pair*/'\uD83D\uDC4D'}</p>
                                                </Image>
                                            )}
                                            {this.state.status.hum === false && (
                                                <Image animate resources={Scream}>
                                                    <p>{'\uD83D\uDC4E'}</p>
                                                </Image>
                                            )}
                                        </Content>
                                    </Frame>
                                </Col>
                            </Row>
                        </div>
                    )}
                </Arwes>
            </ThemeProvider>
        );
    }
}

export default withRouter(Home);