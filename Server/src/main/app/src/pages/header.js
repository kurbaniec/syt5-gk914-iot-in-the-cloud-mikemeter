import * as React from "react";
import {
    ThemeProvider, createTheme, Button, Arwes, Row, Col, Content,
    Frame, Words, Header as AresHeader, Heading
} from 'arwes';
import '../style.css'
import { withRouter, Redirect } from "react-router-dom";

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <AresHeader animate show={this.props.anim.entered}>
                <Row>
                    <Col>
                        <h1 className="Title" onClick={() => this.props.history.push("/")}>
                            MikeMeter
                        </h1>
                    </Col>
                    <Col style={{marginTop: 18}}>
                        <Heading className="Link" node='h2' onClick={() => this.props.history.push("/temperature")}>
                            Temperature
                        </Heading>
                        <Heading className="Link" node='h2' onClick={() => this.props.history.push("/humidity")}>
                            Humidity
                        </Heading>
                    </Col>
                </Row>
            </AresHeader>
        );
    }
}

export default (Header);