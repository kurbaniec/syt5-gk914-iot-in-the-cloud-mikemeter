importScripts("modules/dist/extended-lib.js");

/**
 * Setup environment
 */
const prod = process.env.NODE_ENV === 'production';
const testSpring = process.env.SPRING === 'true';
let serverPath = '';
if (testSpring) {
    serverPath = 'http://localhost:8080';
}

let tmpTH = 20;
let humTH = 25;
let rate = 3000;
let intervalID;
let timestamp = new Date(new Date().setHours(0,0,0,0));


self.addEventListener('message', async function(e) {
    const cmd = e.data[0];
    const data = e.data[1];
    switch (cmd) {
        case 'home':
            home();
            break;
        case 'tmp':
            temperature();
            break;
        case 'hum':
            humidity();
            break;
        case 'time':
            timestamp = data;
            break;
        case 'clear':
            clearAsyncInterval(intervalID);
            break;
        case 'config':
            config(data);
            break;
        case 'getConfig':
            getConfig();
            break;
        case 'echo':
            self.postMessage(['echo', data]);
            break;
        default:
            self.postMessage(['error', 'Unknown command: ' + data.msg]);
    }
}, false);


const setTresholds = async (newTmp, newHum) => {
    if (prod || testSpring) {
        try {
            let response = await fetch(serverPath + '/tmpth', {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newTmp)
            });
            tmpTH = await response.json();
            response = await fetch(serverPath + '/humth', {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newHum)
            });
            humTH = await response.json();
        } catch (e) {
            console.log("MikeMeter-Service down", e);
        }
    } else {
        tmpTH = 20;
        humTH = 25;
    }
};

const getTresholds = async () => {
    await setTresholds(0, 0);
};

const setRate = async (newRate) => {
    if (prod || testSpring) {
        try {
            let response = await fetch(serverPath + '/rate', {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newRate)
            });
            rate = await response.json();
        } catch (e) {
            console.log("MikeMeter-Service down", e);
        }
    } else {
        rate = 3000;
    }
};

const getRate = async () => {
    await setRate(0);
};

const influxQuery = async (type, time, last) => {
    if (prod || testSpring) {

        if (!last) time = time.toISOString();
        let response = await fetch(serverPath + '/query', {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                type: type,
                time: time,
                last: last
            })
        });
        const data = await response.json();
        if (data.results[0].hasOwnProperty('series') && data.results[0].series !== null) {
            return last ? data.results[0].series[0].values[0] : data.results[0].series[0].values;
        } else {
            return null;
        }

    } else {
        const selector = last ? 'LAST(*)' : '*';
        const timing = last ? '' : 'WHERE time >= \'' + time.toISOString() + '\'';
        const query = 'q=SELECT ' + selector + ' FROM "' + type + '" ' + timing;
        const response = await fetch(process.env.INFLUXDEV + '/query?db=mydb', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: query
        });
        const data = await response.json();
        if (data.results[0].hasOwnProperty('series')) {
            return last ? data.results[0].series[0].values[0] : data.results[0].series[0].values;
        } else {
            return null;
        }
    }
};

const home = () => {
    let status = null;
    let cache = null;
    intervalID = setAsyncInterval(async () => {
        try {
            await getTresholds();
            let tmpStatus = influxQuery("tmp", "", true);
            let humStatus = influxQuery("hum", "", true);
            tmpStatus = await tmpStatus;
            humStatus = await humStatus;
            status = {
                tmp: (tmpStatus === null) ? null : tmpStatus[1] < tmpTH,
                hum: (humStatus === null) ? null : humStatus[1] < humTH
            };
            if (!equalStatus(status, cache)) {
                cache = status;
                self.postMessage(['home', status]);
            }
        } catch (e) {
            console.log("MikeMeter-Service down", e);
        }
    }, 1000);
};

const temperature = () => {
    let data = null;
    let cache = null;
    intervalID = setAsyncInterval(async () => {
        let err = false;
        await getTresholds();
        let readings = influxQuery("tmp", timestamp, false);
        let tmpStatus = influxQuery("tmp", "", true);
        readings = await readings.catch(() => err = true );
        tmpStatus = await tmpStatus.catch(() => err = true );
        if (!err) {
            const time = [];
            const value = [];
            if (readings !== null) {
                readings.map(x => {
                    time.push(x[0]);
                    value.push(x[1]);
                });
            }
            readings = {
                time: time,
                value: value
            };
            data = {
                status: tmpStatus[1] < tmpTH,
                readings: readings
            };
            if (!equalData(data, cache)) {
                cache = data;
                self.postMessage(['tmp', data]);
            }
        } else console.log("MikeMeter-Service down");
    }, 1000);
};

const humidity = () => {
    let data = null;
    let cache = null;
    intervalID = setAsyncInterval(async () => {
        let err = false;
        await getTresholds();
        let readings = influxQuery("hum", timestamp, false);
        let volStatus = influxQuery("hum", "", true);
        readings = await readings.catch(() => err = true );
        volStatus = await volStatus.catch(() => err = true );
        if(!err) {
            const time = [];
            const value = [];
            if (readings !== null) {
                readings.map(x => {
                    time.push(x[0]);
                    value.push(x[1]);
                });
            }
            readings = {
                time: time,
                value: value
            };
            data = {
                status: volStatus[1] < humTH,
                readings: readings
            };
            if (!equalData(data, cache)) {
                cache = data;
                self.postMessage(['hum', data]);
            }
        } else console.log("MikeMeter-Service down");
    }, 1000);
};

const config = async (data) => {
    await setTresholds(data.tmpTH, data.humTH);
    await setRate(data.rate);
    await getConfig();
};

const getConfig = async () => {
    await getTresholds();
    await getRate();
    const data = {
        tmpTH: tmpTH,
        humTH: humTH,
        rate: rate
    };
    self.postMessage(['config', data]);
};