import React from "react";
import ReactDOM from "react-dom";
import {
    BrowserRouter as Router,
    Switch,
    Redirect,
    Route
} from "react-router-dom";
import Home from "./pages/home";
import Temperature from "./pages/temperature";
import Volume from "./pages/volume";
import Config from "./pages/config";
import Humidity from "./pages/humidity";


class App extends React.Component {

    constructor(state) {
        super(state);
        this.state = {
          worker: new Worker('worker.js')
        };
    }

    componentDidCatch(error, errorInfo) {
        console.log(error, errorInfo);
    }

    getApp() {
        return (
            <Router>
            <Switch>
                {/* Home route */}
                <Route exact path="/"
                   render={(props) => <Home worker={this.state.worker}/>}/>
                {/* Temperature route*/}
                <Route exact path="/temperature"
                       render={(props) => <Temperature worker={this.state.worker}/>}/>
                {/* Volume route*/}
                <Route exact path="/humidity"
                       render={(props) => <Humidity worker={this.state.worker}/>}/>
                {/* Config route*/}
                <Route exact path="/admin"
                       render={(props) => <Config worker={this.state.worker}/>}/>
            </Switch>
            </Router>
        );
    }

    render() {
        return this.getApp();
    }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);