const generateLayout = (type) => {
    let title;
    let ytitle;
    if (type === 'tmp') {
        title = 'Temperature Measurements';
        ytitle = 'Temperature in °C';
    } else if (type === 'hum') {
        title = 'Humidity Measurements';
        ytitle = 'Humidity in %';
    } else {
        title = 'Volume Measurements';
        ytitle = 'Volume in dB';
    }
    return {
        title: title,
        font: {
            color: '#26dafd'
        },
        paper_bgcolor: 'rgba(0,0,0,0)',
        plot_bgcolor: 'rgba(0,0,0,0)',
        xaxis: {
            title: 'Time',
            showgrid: false,
            zeroline: false
        },
        yaxis: {
            title: ytitle,
            showline: false
        }
    }
};

const lineColor =  {
    color: 'yellow',
};

const layout = {
    title: 'Values',
    font: {
        color: '#26dafd'
    },
    paper_bgcolor: 'rgba(0,0,0,0)',
    plot_bgcolor: 'rgba(0,0,0,0)',
    xaxis: {
        title: 'Time',
        showgrid: false,
        zeroline: false
    },
    yaxis: {
        title: 'Val',
        showline: false
    }
};

const tmpLayout = generateLayout('tmp');
const volLayout = generateLayout('vol');
const humLayout = generateLayout('hum');

export {layout, tmpLayout, volLayout, humLayout, lineColor};