var sleep=async function(a){return new Promise(function(b){return setTimeout(b,a)})},isReachable=async function(a){var b=new AbortController,c=b.signal,d=fetch(a,{signal:c}),e=setTimeout(function(){return b.abort()},5e3);d.then(function(){return!0}).catch(function(){return!1})},asyncIntervals=[],runAsyncInterval=async function(a,b,c){await a(),asyncIntervals[c]&&setTimeout(function(){return runAsyncInterval(a,b,c)},b)},setAsyncInterval=function(a,b){if(a&&"function"==typeof a){var c=asyncIntervals.length;return asyncIntervals.push(!0),runAsyncInterval(a,b,c),c}throw new Error("Callback must be a function")},clearAsyncInterval=function(a){asyncIntervals[a]&&(asyncIntervals[a]=!1)},equalStatus=function(a,b){return!(null!==a||null!==b)||(null===a||null!==b)&&(null!==a||null===b)&&a.tmp===b.tmp&&a.hum===b.hum},equalData=function(a,b){if(null===b||b===void 0)return!1;if(a.status!==b.status)return!1;if(a.readings.time.length===b.readings.time.length&&a.readings.value.length===b.readings.value.length){for(var c=a.readings.time.length;c--;)if(a.readings.time[c]!==b.readings.time[c])return!1;for(var d=a.readings.value.length;d--;)if(a.readings.value[d]!==b.readings.value[d])return!1;return!0}return!1};