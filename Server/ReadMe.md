# WebService

## Build React-App

```
gradle appInstall
```

```
gradle appBuild
```

```
gradle appCopy
```

## Start Spring-Boot-Application

```
gradle bootRun
```

## Add Measurement to Datastore

```
curl -i -X POST -H "Content-Type: application/json" -d "{\"time\": 0, \"tmp\": 21.0, \"vol\": 81.0}" http://localhost:8080/add
curl -i -X POST -H "Content-Type: application/json" -d "{\"time\": 0, \"tmp\": 22.0, \"vol\": 82.0}" http://localhost:8080/add
curl -i -X POST -H "Content-Type: application/json" -d "{\"time\": 0, \"tmp\": 23.0, \"vol\": 83.0}" http://localhost:8080/add
curl -i -X POST -H "Content-Type: application/json" -d "{\"time\": 0, \"tmp\": 24.0, \"vol\": 84.0}" http://localhost:8080/add
curl -i -X POST -H "Content-Type: application/json" -d "{\"time\": 0, \"tmp\": 25.0, \"vol\": 85.0}" http://localhost:8080/add

curl -i -X POST -H "Content-Type: application/json" -d "{\"type\": \"tmp\", \"time\": \"\", \"last\": true}" http://localhost:8080/query
curl -i -X POST -H "Content-Type: application/json" -d "{\"type\": \"vol\", \"time\": \"\", \"last\": true}" http://localhost:8080/query
curl -i -X POST -H "Content-Type: application/json" -d "{\"type\": \"tmp\", \"time\": \"2019-04-13T15:48:16Z\", \"last\": false}" http://localhost:8080/query
curl -i -X POST -H "Content-Type: application/json" -d "{\"type\": \"vol\", \"time\": \"2019-04-13T15:48:16Z\", \"last\": false}" http://localhost:8080/query

curl -i -X POST -H "Content-Type: application/json" -d "3000" http://localhost:8080/rate
```


## Sources

* [Spring-Boot + React](https://blog.indrek.io/articles/serving-react-apps-from-spring-boot/)
* [Gradle Node Plugin](https://github.com/srs/gradle-node-plugin/blob/master/docs/node.md)
* [React + WebAssembly](https://prestonrichey.com/blog/react-rust-wasm/)
* [Spring-Boot fix mime-type](https://stackoverflow.com/questions/47222191/spring-boot-static-resources-and-mime-type-configuration?rq=1)