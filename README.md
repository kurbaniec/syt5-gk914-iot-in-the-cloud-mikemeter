# Embedded Systems "IoT in der Cloud"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

Teammitglieder: Sarah Breit, Fabio Fuchs, Kacper Urbaniec, Martin Wustinger

## Aufteilung im Team
### Raspberry Pi (Fabio und Sarah)
* Sensordaten lesen (Temperatur und Lautstärke)
* Daten bereitstellen und post an Server
* Aktoren implementieren (Lautsprecher)
* Optional Kamera (OpenCV Facerecognition)

### Server (Kacper und Martin)

* Spring Backend
* Influx-DataStore
* Web-Visualisierung

## Recherche
### Protokoll
[Overleaf Protokoll](https://www.overleaf.com/read/swtynpgqrmqz)

## Server-Deployment
### :whale: With Docker (Recommended)

If you just want to test Mikemeter locally, build the Docker image and run it.

```
cd Server
sudo docker-compose build
sudo docker-compose up
```

You can tweak the MikeMeter configuration in the `docker-compose` file, with the following environment variables:

* `HTTPS_PORT`

  Set the https port of the Server.

* `HTTP_PORT`

  Set the http port of the Server.


When using https a self-signed certificate will be used as default.

If you want to use your own certificate, replace the `mikemeter_cert.p12`  certificate in the `_certificates` folder with your own one. 

> Note: The certificate should be named `mikemeter_cert.p12`, have the key-alias `mikemeter_cert`, password `changeit` and use the `PKCS 12` format.

Now you can build and run the Easylist service:

```
cd Server
sudo docker-compose build
sudo docker-compose up
```

### :elephant: With Gradle

Adapt the service to your infrastructure via the `application.properties` files found under `src/main/resoures` in the `Server` folder.

Then you can start the service with:

```
gradle appInstall && gradle appBuild && gradle appCopy && gradle bootRun
```

