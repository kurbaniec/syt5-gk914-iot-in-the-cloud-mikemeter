# Schlachtplan Mikemeter

Sensoren:

+ Bewegungssensor
+ Kamera

Aktoren:

+ Lautsprecher

Funktionen:

+ Borko ist im Labor (optional)
+ Borko Haß-Meter
+ DeziBorko-Messer
+ Wee-Wuu-Wee-Wuu (optional)

Technologien:

+ InfluxDB am Spring-Server
+ Spring REST-Schnittstelle für Daten Anzeige und Speicherung
+ Graphisches Interface zur Datenanzeige

Plan:

SpringBoot Server mit InfluxDB

InfluxDB am Server--> Raspi schickt Daten an InfluxDB über REST-Schnittstelle

Analyse von Daten auf Grafischer Oberfläche

